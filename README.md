# Weathr #

Weathr is an iOS app that lets you view the current and forecasted weather in any city.

Weathr uses the [OpenWeatherMap API](http://openweathermap.org/) to obtain all weather information.

![weathr-ss-1.png](https://bitbucket.org/repo/rLoz6A/images/3895094774-weathr-ss-1.png)
![weathr-ss-2.png](https://bitbucket.org/repo/rLoz6A/images/4113201667-weathr-ss-2.png)
![weathr-ss-3.png](https://bitbucket.org/repo/rLoz6A/images/1767699637-weathr-ss-3.png)


## Build Instructions ##
Navigate to *Downloads* (on the left side panel). Click *Download* to begin downloading a zipped version of the repository.

Once the download has finished, unzip the repository by double-clicking the file. Inside the *weathr* folder, open the file named *weathr.xcworkspace*. The file will be opened in Xcode. To run *weathr*, while in Xcode click the play button near the top left of your screen.