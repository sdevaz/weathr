//
//  WPlace.m
//  weathr
//
//  Created by Sage De Vaz on 2016-09-01.
//  Copyright © 2016 Morph Media. All rights reserved.
//

#import "WPlace.h"

@implementation WPlace

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"name": @"name",
             @"country": @"countryName",
             };
}

@end
