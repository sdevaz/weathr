//
//  WService.m
//  weathr
//
//  Created by Sage De Vaz on 2016-08-31.
//  Copyright © 2016 Morph Media. All rights reserved.
//

#import "WService.h"
#import <Mantle/Mantle.h>

static NSString * const GNAPIkey = @"weathr";
static NSString * const OWMAPIkey = @"87cfb2543fd5490c2d9453c56943bc1d";
static NSString * const units = @"metric";

@interface WService()

@property (strong, nonatomic) NSURLSession *session;

@end


//http://api.geonames.org/searchJSON?q=surrey&username=weathr

@implementation WService

+ (instancetype)sharedManager {
    static id _sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedManager = [[self alloc] init];
    });
    
    return _sharedManager;
}

- (id)init {
    if (self = [super init]) {
        NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
        _session = [NSURLSession sessionWithConfiguration:config];
    }
    return self;
}

- (void)placesForPlaceName:(NSString*)placeName WithCompletionHandler:(void (^)(NSArray *places))completionHandler failureHandler:(void (^)(NSError * error))failureHandler {
    
    NSString *encodedPlaceString = [placeName stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLPathAllowedCharacterSet]];

    NSString *urlString = [NSString stringWithFormat:@"http://api.geonames.org/searchJSON?q=%@&username=%@", encodedPlaceString, GNAPIkey];
    NSURL *url = [NSURL URLWithString:urlString];
     NSURLSessionDataTask *dataTask = [self.session dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
         NSError *jsonError = nil;
         id jsonResponse = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&jsonError];
         NSLog(@"response: %@", jsonResponse);
         
         if (error) {
             failureHandler(error);
         } else {
             NSArray *placesListResponse = [jsonResponse objectForKey:@"geonames"];
             NSMutableArray *placesModels = [NSMutableArray new];
             for (NSDictionary *placeJson in placesListResponse) {
                 NSError *mantleError = nil;
                 WPlace *place = [MTLJSONAdapter modelOfClass:[WPlace class] fromJSONDictionary:placeJson error:&mantleError];
                 if (mantleError) {
                     NSLog(@"mantle error: %@", mantleError.localizedDescription);
                     failureHandler(jsonError);
                 }
                 
                 [placesModels addObject:place];
             }
             
             completionHandler([NSArray arrayWithArray:placesModels]);
         }

         
     }];
    
    [dataTask resume];
}

- (void)currentWeatherForLocation:(NSString*)locationString WithCompletionHandler:(void (^)(WWeather * weather))completionHandler failureHandler:(void (^)(NSError * error))failureHandler {
    NSString *urlString = [NSString stringWithFormat:@"http://api.openweathermap.org/data/2.5/weather?q=%@&units=%@&APPID=%@", locationString, units, OWMAPIkey];
    NSURL *url = [NSURL URLWithString:urlString];
    
    NSURLSessionDataTask *dataTask = [self.session dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
            failureHandler(error);
        } else {
            NSError *jsonError = nil;
            id jsonResponse = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&jsonError];
            if (jsonError) {
                failureHandler(jsonError);
            } else {
                NSError *mantleError = nil;
                WWeather *weatherModel = [MTLJSONAdapter modelOfClass:[WWeather class] fromJSONDictionary:jsonResponse error:&mantleError];
                if (mantleError) {
                    NSLog(@"mantle error: %@", mantleError.localizedDescription);
                    failureHandler(jsonError);
                }
                completionHandler(weatherModel);
            }
        }
    }];
    
    [dataTask resume];
}

- (void)hourlyForecastForLocation:(NSString*)locationString WithCompletionHandler:(void (^)(NSArray *forecastList))completionHandler failureHandler:(void (^)(NSError * error))failureHandler {
    NSString *urlString = [NSString stringWithFormat:@"http://api.openweathermap.org/data/2.5/forecast?q=%@&units=%@&APPID=%@", locationString, units, OWMAPIkey];
    NSURL *url = [NSURL URLWithString:urlString];
    
    NSURLSessionDataTask *dataTask = [self.session dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
            failureHandler(error);
        } else {
            NSError *jsonError = nil;
            id jsonResponse = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&jsonError];
            if (jsonError) {
                failureHandler(jsonError);
            } else {
                NSArray *forecastListResponse = [jsonResponse objectForKey:@"list"];
                NSMutableArray *forecastModels = [NSMutableArray new];
                for (NSDictionary *hourlyForecast in forecastListResponse) {
                    NSError *mantleError = nil;
                    WForecast *forecastModel = [MTLJSONAdapter modelOfClass:[WForecast class] fromJSONDictionary:hourlyForecast error:&mantleError];
                    if (mantleError) {
                        NSLog(@"mantle error: %@", mantleError.localizedDescription);
                        failureHandler(jsonError);
                    }
                    
                    [forecastModels addObject:forecastModel];
                }
                
                completionHandler([NSArray arrayWithArray:forecastModels]);

            }
        }
    }];
    
    [dataTask resume];
}

- (void)dailyForecastForLocation:(NSString*)locationString WithCompletionHandler:(void (^)(NSArray *forecastList))completionHandler failureHandler:(void (^)(NSError * error))failureHandler {
    NSString *urlString = [NSString stringWithFormat:@"http://api.openweathermap.org/data/2.5/forecast/daily?q=%@&units=%@&APPID=%@", locationString, units, OWMAPIkey];
    NSURL *url = [NSURL URLWithString:urlString];
    
    NSURLSessionDataTask *dataTask = [self.session dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
            failureHandler(error);
        } else {
            NSError *jsonError = nil;
            id jsonResponse = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&jsonError];
            if (jsonError) {
                failureHandler(jsonError);
            } else {
                NSArray *forecastListResponse = [jsonResponse objectForKey:@"list"];
                NSMutableArray *forecastModels = [NSMutableArray new];
                for (NSDictionary *dailyForecast in forecastListResponse) {
                    NSError *mantleError = nil;
                    WForecast *forecastModel = [MTLJSONAdapter modelOfClass:[WForecast class] fromJSONDictionary:dailyForecast error:&mantleError];
                    if (mantleError) {
                        NSLog(@"mantle error: %@", mantleError.localizedDescription);
                        failureHandler(jsonError);
                    }
                    
                    [forecastModels addObject:forecastModel];
                }
                
                completionHandler([NSArray arrayWithArray:forecastModels]);
            }
        }
    }];
    
    [dataTask resume];
}

@end
