//
//  WPlace.h
//  weathr
//
//  Created by Sage De Vaz on 2016-09-01.
//  Copyright © 2016 Morph Media. All rights reserved.
//

#import <Mantle/Mantle.h>

@interface WPlace : MTLModel <MTLJSONSerializing>

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *country;

@end
