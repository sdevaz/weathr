//
//  WForecastTableViewCell.h
//  weathr
//
//  Created by Sage De Vaz on 2016-08-30.
//  Copyright © 2016 Morph Media. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WForecastTableViewCell : UITableViewCell

- (void)configureWithDate:(NSDate*)date
                    icon:(NSString*)iconNameString
                    high:(NSNumber*)high
                     low:(NSNumber*)low;

@end
