//
//  WForecastTableViewCell.m
//  weathr
//
//  Created by Sage De Vaz on 2016-08-30.
//  Copyright © 2016 Morph Media. All rights reserved.
//

#import "WForecastTableViewCell.h"

@interface WForecastTableViewCell()
@property (weak, nonatomic) IBOutlet UILabel *dayLabel;
@property (weak, nonatomic) IBOutlet UIImageView *iconImageView;
@property (weak, nonatomic) IBOutlet UILabel *temperatureHighLabel;
@property (weak, nonatomic) IBOutlet UILabel *temperatureLowLabel;
@property (nonatomic, strong) NSDateFormatter *dailyFormatter;
@end

@implementation WForecastTableViewCell

-(NSDateFormatter *)dailyFormatter {
    if (!_dailyFormatter) {
        _dailyFormatter = [[NSDateFormatter alloc] init];
        _dailyFormatter.dateFormat = @"EEEE";
    }
    
    return _dailyFormatter;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    //See XIB file for layout and UI configurations
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)configureWithDate:(NSDate*)date
                    icon:(NSString*)iconNameString
                    high:(NSNumber*)high
                     low:(NSNumber*)low {
    
    self.dayLabel.text = [self.dailyFormatter stringFromDate:date];
    self.temperatureHighLabel.text = [NSString stringWithFormat:@"%.0f", high.floatValue];
    self.temperatureLowLabel.text = [NSString stringWithFormat:@"%.0f", low.floatValue];;
    self.iconImageView.image = [UIImage imageNamed:iconNameString];
}

@end
