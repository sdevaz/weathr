//
//  WService.h
//  weathr
//
//  Created by Sage De Vaz on 2016-08-31.
//  Copyright © 2016 Morph Media. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "WWeather.h"
#import "WForecast.h"
#import "WPlace.h"

@interface WService : NSObject

+ (instancetype)sharedManager;

//Location

- (void)placesForPlaceName:(NSString*)placeName WithCompletionHandler:(void (^)(NSArray *places))completionHandler failureHandler:(void (^)(NSError * error))failureHandler;


//Weather

- (void)currentWeatherForLocation:(NSString*)locationString WithCompletionHandler:(void (^)(WWeather * weather))completionHandler failureHandler:(void (^)(NSError * error))failureHandler;

- (void)hourlyForecastForLocation:(NSString*)locationString WithCompletionHandler:(void (^)(NSArray *forecastList))completionHandler failureHandler:(void (^)(NSError * error))failureHandler;

- (void)dailyForecastForLocation:(NSString*)locationString WithCompletionHandler:(void (^)(NSArray * forecastList))completionHandler failureHandler:(void (^)(NSError * error))failureHandler;

@end
