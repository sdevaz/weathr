//
//  WMainViewController.m
//  weathr
//
//  Created by Sage De Vaz on 2016-08-30.
//  Copyright © 2016 Morph Media. All rights reserved.
//

#import "WMainViewController.h"
#import "WTheme.h"
#import "WForecastTableViewCell.h"
#import "WHourlyForecastCollectionViewCell.h"
#import "WService.h"
#import "WWeather.h"
#import "WSearchViewController.h"

static NSString * const forecastCellID = @"WForecastTableViewCell";
static NSString * const hourlyForecastCellID = @"WHourlyForecastCollectionViewCell";


@interface WMainViewController () <UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate, UICollectionViewDelegate, UICollectionViewDataSource>
@property (nonatomic, assign) CGSize screenSize;

@property (nonatomic, strong) WSearchViewController *searchVC;
@property (nonatomic, strong) UIImageView *backgroundImageView;

//current weather view
@property (nonatomic, strong) UIView *currentWeatherView;
@property (nonatomic, strong) UIButton *cityButton;
@property (nonatomic, strong) UILabel *currentTemperatureLabel;
@property (nonatomic, strong) UILabel *currentConditionLabel;

//hourly forecast
@property (nonatomic, strong) NSArray *hourlyForecasts;
@property (nonatomic, strong) UICollectionView *collectionView;

//daily forecast
@property (nonatomic, strong) NSArray *dailyForecasts;
@property (nonatomic, strong) UITableView *tableView;

@end

@implementation WMainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.searchVC = [[WSearchViewController alloc] init];
    self.searchVC.mainVC = self;
    
    self.screenSize = [UIScreen mainScreen].bounds.size;
    
    [self setupBackgroundImageView];
    [self setupTableView];
    [self setupCurrentWeatherView];
    
    //Add observer so that we can refresh our data when the app is resumed
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(fetchWeatherData) name:UIApplicationWillEnterForegroundNotification object:nil];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillEnterForegroundNotification object:nil];
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    
    CGRect bounds = self.view.bounds;
    
    self.tableView.frame = bounds;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self fetchWeatherData];
}

-(NSString *)placeName {
    if (!_placeName || !_placeName.length) {
        //We use placeName to query weather. Defaulting to "Vancouver, Canada";
        _placeName = @"Vancouver,Canada";
    }
    
    return _placeName;
}

- (void)fetchWeatherData {
    WService *service = [WService sharedManager];
    
    [service  currentWeatherForLocation:self.placeName WithCompletionHandler:^(WWeather * weather) {
        [service hourlyForecastForLocation:self.placeName WithCompletionHandler:^(NSArray *forecastList) {
            self.hourlyForecasts = forecastList;
            [service  dailyForecastForLocation:self.placeName WithCompletionHandler:^(NSArray * forecastList) {
                self.dailyForecasts = forecastList;
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self updateCurrentWeatherViewWithWeather:weather];
                    [self.collectionView reloadData];
                    [self.tableView reloadData];
                });
            } failureHandler:^(NSError *error) {
                //
            }];
        } failureHandler:^(NSError *error) {
            //
        }];
        
    } failureHandler:^(NSError *error) {
        
    }];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

#pragma mark - View Setup

- (void)setupBackgroundImageView {
    self.view.backgroundColor = [WTheme backgroundColor];
    CGRect backgroundFrame = CGRectMake(0, 0, self.screenSize.width, self.screenSize.height);
    self.backgroundImageView = [[UIImageView alloc] initWithFrame:backgroundFrame];
    self.backgroundImageView.image = [UIImage imageNamed:@"earth-space_bg"];
    self.backgroundImageView.contentMode = UIViewContentModeCenter;
    [self.view addSubview:self.backgroundImageView];
}

- (void)setupCurrentWeatherView {
    
    self.currentWeatherView = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    
    CGFloat startY = 60;
    CGFloat marginY = 8;
    
    CGRect cityButtonFrame = CGRectMake(0, startY, self.screenSize.width, 50);
    self.cityButton = [[UIButton alloc] initWithFrame:cityButtonFrame];
    [self.cityButton addTarget:self action:@selector(tappedCityButton) forControlEvents:UIControlEventTouchUpInside];
    self.cityButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.cityButton.titleLabel.font = [UIFont systemFontOfSize:34];
    [self.cityButton setImage:[UIImage imageNamed:@"icon_settings"] forState:UIControlStateNormal];
    [self.cityButton setTitleColor:[WTheme textColorPrimary] forState:UIControlStateNormal];
    [self.currentWeatherView addSubview:self.cityButton];
    
    CGFloat currentTemperatureY = startY + cityButtonFrame.size.height + marginY;
    CGRect currentTemperatureFrame = CGRectMake(0, currentTemperatureY, self.screenSize.width, 100);
    self.currentTemperatureLabel = [[UILabel alloc] initWithFrame:currentTemperatureFrame];
    self.currentTemperatureLabel.textAlignment = NSTextAlignmentCenter;
    self.currentTemperatureLabel.font = [UIFont boldSystemFontOfSize:100];
    self.currentTemperatureLabel.textColor = [WTheme textColorPrimary];
    [self.currentWeatherView addSubview:self.currentTemperatureLabel];
    
    CGFloat currentConditionY = currentTemperatureY + currentTemperatureFrame.size.height + marginY;
    CGRect currentConditionFrame = CGRectMake(0, currentConditionY, self.screenSize.width, 40);
    self.currentConditionLabel = [[UILabel alloc] initWithFrame:currentConditionFrame];
    self.currentConditionLabel.textAlignment = NSTextAlignmentCenter;
    self.currentConditionLabel.font = [UIFont systemFontOfSize:28];
    self.currentConditionLabel.textColor = [WTheme textColorPrimary];
    [self.currentWeatherView addSubview:self.currentConditionLabel];
    
    NSString *city = [self.placeName componentsSeparatedByString:@","].firstObject;
    [self.cityButton setTitle:city forState:UIControlStateNormal];
    self.currentTemperatureLabel.text = @"";
    self.currentConditionLabel.text = @"";
    
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    layout.itemSize = CGSizeMake(80, 80);
    CGFloat collectionViewHeight = 120;
    CGRect collectionViewFrame = CGRectMake(0, self.screenSize.height - collectionViewHeight, self.screenSize.width, collectionViewHeight);
    self.collectionView = [[UICollectionView alloc] initWithFrame:collectionViewFrame collectionViewLayout:layout];
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    self.collectionView.backgroundColor = [UIColor clearColor];
    
    UINib *hourlyForecastNib = [UINib nibWithNibName:hourlyForecastCellID bundle:nil];
    [self.collectionView registerNib:hourlyForecastNib forCellWithReuseIdentifier:hourlyForecastCellID];
    [self.currentWeatherView addSubview:self.collectionView];
    
    self.tableView.tableHeaderView = self.currentWeatherView;
}

- (void)setupTableView {
    self.tableView = [[UITableView alloc] init];
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.separatorColor = [UIColor colorWithWhite:1 alpha:0.2];
    self.tableView.pagingEnabled = YES;
    
    UINib *forecastTableViewCellNib = [UINib nibWithNibName:@"WForecastTableViewCell" bundle:nil];
    [self.tableView registerNib:forecastTableViewCellNib forCellReuseIdentifier:forecastCellID];
    
    [self.view addSubview:self.tableView];
}

#pragma mark - View Update

- (void)updateCurrentWeatherViewWithWeather:(WWeather*)weather {
    [self.cityButton setTitle:weather.locationName forState:UIControlStateNormal];
    self.currentConditionLabel.text = weather.weatherDescription;
    self.currentTemperatureLabel.text = [NSString stringWithFormat:@"%.0f°",[weather.temperature floatValue]];
    [self.tableView setTableHeaderView:self.currentWeatherView];
}

#pragma mark - CollectionView DataSource

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    //displaying latest 6 hourly forecasts
    return MIN((self.hourlyForecasts.count), 6);
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    WForecast *hourlyForecast = [self.hourlyForecasts objectAtIndex:indexPath.row];
    WHourlyForecastCollectionViewCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:hourlyForecastCellID forIndexPath:indexPath];
    [cell configureWithDate:hourlyForecast.date imageName:[hourlyForecast imageName] temperature:hourlyForecast.temperature];
    return cell;
}

#pragma mark - TableView DataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dailyForecasts.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    WForecast *dailyForecast = [self.dailyForecasts objectAtIndex:indexPath.row];
    
    WForecastTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:forecastCellID forIndexPath:indexPath];
    [cell configureWithDate:dailyForecast.date icon:[dailyForecast imageName] high:dailyForecast.tempHigh low:dailyForecast.tempHigh];
    return cell;
}

#pragma mark - TableView Delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSInteger cellCount = [self tableView:tableView numberOfRowsInSection:indexPath.section];
    return self.screenSize.height / (CGFloat)cellCount;
}

#pragma mark - IBACTIONs

- (void)tappedCityButton {
    [self presentViewController:self.searchVC animated:YES completion:nil];
}

@end
