//
//  WMainViewController.h
//  weathr
//
//  Created by Sage De Vaz on 2016-08-30.
//  Copyright © 2016 Morph Media. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WMainViewController : UIViewController
@property (nonatomic, strong) NSString *placeName;
@end
