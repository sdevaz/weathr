//
//  WTheme.m
//  weathr
//
//  Created by Sage De Vaz on 2016-08-30.
//  Copyright © 2016 Morph Media. All rights reserved.
//

#import "WTheme.h"

@implementation WTheme

+ (UIColor*)textColorPrimary {
    return [UIColor whiteColor];
}

+ (UIColor*)backgroundColor {
    return [UIColor colorWithRed:39/255.0 green:39/255.0 blue:39/255.0 alpha:1.0];
}

@end
