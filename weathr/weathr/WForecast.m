//
//  WForecast.m
//  weathr
//
//  Created by Sage De Vaz on 2016-08-31.
//  Copyright © 2016 Morph Media. All rights reserved.
//

#import "WForecast.h"

@implementation WForecast

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    NSMutableDictionary *paths = [[super JSONKeyPathsByPropertyKey] mutableCopy];
    paths[@"tempHigh"] = @"temp.max";
    paths[@"tempLow"] = @"temp.min";
    return paths;
}

@end
