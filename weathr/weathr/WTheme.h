//
//  WTheme.h
//  weathr
//
//  Created by Sage De Vaz on 2016-08-30.
//  Copyright © 2016 Morph Media. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface WTheme : NSObject

+ (UIColor*)textColorPrimary;
+ (UIColor*)backgroundColor;

@end
