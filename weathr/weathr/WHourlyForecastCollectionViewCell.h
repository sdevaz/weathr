//
//  WHourlyForecastCollectionViewCell.h
//  weathr
//
//  Created by Sage De Vaz on 2016-09-01.
//  Copyright © 2016 Morph Media. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WHourlyForecastCollectionViewCell : UICollectionViewCell

- (void)configureWithDate:(NSDate*)date imageName:(NSString*)imageName temperature:(NSNumber*)temperature;

@end
