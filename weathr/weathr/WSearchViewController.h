//
//  WSearchViewController.h
//  weathr
//
//  Created by Sage De Vaz on 2016-09-01.
//  Copyright © 2016 Morph Media. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WMainViewController.h"

@interface WSearchViewController : UIViewController
@property (nonatomic, weak) IBOutlet UISearchBar *searchBar;
@property (nonatomic, weak) WMainViewController *mainVC;
@end
