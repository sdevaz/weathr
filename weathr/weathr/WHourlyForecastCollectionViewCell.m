//
//  WHourlyForecastCollectionViewCell.m
//  weathr
//
//  Created by Sage De Vaz on 2016-09-01.
//  Copyright © 2016 Morph Media. All rights reserved.
//

#import "WHourlyForecastCollectionViewCell.h"

@interface WHourlyForecastCollectionViewCell()
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *temperatureLabel;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (nonatomic, strong) NSDateFormatter *hourlyFormatter;
@end

@implementation WHourlyForecastCollectionViewCell

-(NSDateFormatter *)hourlyFormatter {
    if (!_hourlyFormatter) {
        _hourlyFormatter = [[NSDateFormatter alloc] init];
        _hourlyFormatter.dateFormat = @"h a";
    }
    
    return _hourlyFormatter;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    //Set up in Nib
}

-(void)configureWithDate:(NSDate *)date imageName:(NSString *)imageName temperature:(NSNumber *)temperature {
    
    self.timeLabel.text = [self.hourlyFormatter stringFromDate:date];
    self.temperatureLabel.text = [NSString stringWithFormat:@"%.0f", temperature.floatValue];
    self.imageView.image = [UIImage imageNamed:imageName];
}

@end
